using System;
using log4net;
using System.Reflection;

namespace Acebook.Model
{
    public class Bet
    {
        public int Id {get;set;}
        public int ContestId {get;set;}
        public int BettorId {get;set;}
        public Decimal WagerAmount {get;set;}
        public bool Settled {get;set;}
    }
}