using System;
using log4net;
using System.Reflection;

namespace Acebook.Model
{
    public class Contest
    {
        public int Id {get;set;}
        public string HomeTeam {get;set;}
        public string VisitingTeam {get;set;}
        public DateTime DateTimeOfContest {get;set;}
        public int? ContestResult {get;set;}
        public int BetIdForHomeTeam{get;set;}
        public int BetIdForVisitingTeam{get;set;}

    }



}