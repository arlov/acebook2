using System;
using log4net;
using System.Reflection;
using System.Collections.Generic;

namespace Acebook.Model
{
    public class AcebookDataModel
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        public List<Bet> Bets {get;set;}
        public List<Bettor> Bettors {get;set;}
        public List<Contest> Contests {get;set;}

        public AcebookDataModel(){
 
            Bets = new List<Bet>();

            Bettors = new List<Bettor>();
            
            Contests = new List<Contest>();
 
        }

        public void DataModelStatus(){
            
            log.Debug($"Number of Bettors = {Bettors.Count}  Number Of Contests = {Contests.Count}  Number Of Bets = {Bets.Count}");

            return;

        }

        public void DataModelDetailDump(){

            log.Debug("Dumping DataModel....");

            log.Debug("Bettors...");

            foreach(Bettor bettor in this.Bettors){

                log.Debug($"Id = {bettor.Id}");
                log.Debug($"Name = {bettor.FirstName} {bettor.LastName}");
                log.Debug($"Phone = {bettor.PhoneNumber}");
                log.Debug($"Email = {bettor.EmailAddress}");

            }

            log.Debug("Contests...");

            foreach(Contest contest in this.Contests){
                
                log.Debug($"Id = {contest.Id}");
                log.Debug($"Date = {contest.DateTimeOfContest}");
                log.Debug($"Home Team = {contest.HomeTeam}({contest.BetIdForHomeTeam})");
                log.Debug($"Visiting Team = {contest.VisitingTeam}({contest.BetIdForVisitingTeam})");
                log.Debug($"Result = {contest.ContestResult}");                                

            }

            log.Debug("Bets...");

            foreach(Bet bet in this.Bets){

                log.Debug($"Id = {bet.Id}");
                log.Debug($"Contest Id = {bet.ContestId}");
                log.Debug($"Bettor Id = {bet.BettorId}");
                log.Debug($"Wager Amount = {bet.WagerAmount}");
                log.Debug($"Settled = {bet.Settled}");   
            }

            return;

        }


    }
}