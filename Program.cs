﻿using log4net;
using log4net.Config;
using System.IO;
using System.Reflection;
using Acebook.Controllers;

namespace Acebook
{
    class Program
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));

            log.Info("Entering Main Execution Loop");

            AceBookController controller = new AceBookController();

            controller.Execute();

            controller.Quit();            

            log.Info("Exiting App With Success");
        }
    }
}
