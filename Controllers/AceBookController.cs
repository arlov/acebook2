using System;
using log4net;
using log4net.Config;
using System.IO;
using System.Reflection;
using Acebook.Model;
using Acebook.DataAccess;

namespace Acebook.Controllers
{
    public class AceBookController
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private AcebookDataModel dataModel;
        private AcebookDataAccessAdapter dataAccessAdapter;

        public AceBookController(){

            dataAccessAdapter = new AcebookDataAccessAdapter();

        }

        public void Execute(){

            log.Info("Executing Acebook Controller...");
            
            dataModel = dataAccessAdapter.LoadDataModel();
            
            dataModel.DataModelDetailDump();

        }

        public void Quit(){

        }

    }
}