using System;
using log4net;
using System.Reflection;
using Acebook.Model;
using System.Linq;

namespace Acebook.DataAccess
{
    public class AcebookDataAccessAdapter
    {
        private static readonly ILog log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private AcebookDataModel dataModel;
        private int Selector;

        public AcebookDataModel LoadDataModel()
        {
            log.Info("Loading Acebook Data Model");
            
            dataModel =  new AcebookDataModel();

            int maxEntities = 5;
            
            AddBettors(maxEntities);
            AddContests(maxEntities);
            AddBets(maxEntities);

            return dataModel;

        }

        private void AddBettors(int numberOfBettors){
            
            string[] BettorNames = {"Dave","John","Clyde","Jim","Tony"};

            Bettor bettor;

            for(int j=0;j< numberOfBettors;j++){

                bettor = new Bettor();

                bettor.FirstName = BettorNames[j];

                dataModel.Bettors.Add(bettor);

            }

            return;

        }

        private void AddContests(int numberOfContests){

            string[] NBATeams = {"LAL","GSW","CHI","NYN","PHI"};
            
            Contest contest;

            for(int j=0;j< numberOfContests;j++){

                contest = new Contest();

                contest.DateTimeOfContest = DateTime.Now;
                contest.HomeTeam = ArrayItemSelector(NBATeams);
                contest.VisitingTeam = ArrayItemSelector(NBATeams);

                dataModel.Contests.Add(contest);

            }

            return;

        }

        private void AddBets(int numberOfBets){
            
            Bet bet;

            for(int j=0;j< numberOfBets;j++){

                bet = new Bet();
                bet.BettorId = this.dataModel.Bettors.First<Bettor>().Id;
                bet.ContestId = this.dataModel.Contests.First<Contest>().Id;
                bet.WagerAmount = 50;
                bet.Settled = false;
                
                dataModel.Bets.Add(bet);

            }

            return;

        }

        private string ArrayItemSelector(string[] arrayOfStrings)
        {
            this.Selector = this.Selector + 1;
            
            if (this.Selector >= arrayOfStrings.Length){
                
                this.Selector = 0;
            }

            return arrayOfStrings[this.Selector];

        }

    }
}